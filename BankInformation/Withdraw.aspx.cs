﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BankInformation
{
	public partial class Withdraw : System.Web.UI.Page
	{
		string connentionstring = @"Server=SHAKIL-IT;Database=Bankdb;User id=sa;Password=123";
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		protected void LinkButton1_Click(object sender, EventArgs e)
		{
			Session.Abandon();
			Response.Redirect("HomePage.aspx");
		}

		protected void savebutton_Click(object sender, EventArgs e)
		{
			SqlConnection connection = new SqlConnection(connentionstring);

			string query = "Update Accounts set balance=balance-@Balance where accountnumber=@AccountNumber";
			SqlCommand command = new SqlCommand(query, connection);
			command.Parameters.AddWithValue("@AccountNumber", txtAccountNo.Text.Trim());
			command.Parameters.AddWithValue("@Balance", txtamount.Text.Trim());
			connection.Open();
			command.ExecuteNonQuery();
			connection.Close();
		}
	}
}